:: Copyright (C) 2020 - Author: Ingo
:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: ARCHIVE_PATH path to the /e/ archive to flash
:: $2: FASTBOOT_FOLDER_PATH: the path where runnable fastboot is stored
:: $3: JAVA_FOLDER_PATH: the path where runnable jar is stored (to unpack ZIP file)

:: Exit status
:: - 0 : /e/ installed
:: - 1 : user data wipe failed
:: - 2 : flashing of a partition failed
:: - 101 : ARCHIVE_PATH missing
:: - 102 : archive could not be unpacked

set ARCHIVE_PATH=%~1
set FASTBOOT_FOLDER_PATH=%~2
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"
set JAVA_FOLDER_PATH=%~3
set JAR_PATH="%JAVA_FOLDER_PATH%\bin\jar"

if not defined %ARCHIVE_PATH (
  exit /b 101
)

for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_NAME="%%~na"
)

for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH="%%~dpa"
)

echo "archive path : "%ARCHIVE_PATH%
echo "archive folder path : "%ARCHIVE_FOLDER_PATH%
echo "fastboot path : "%FASTBOOT_PATH%
echo "jar path : "%JAR_PATH%

cd "%ARCHIVE_FOLDER_PATH%"

timeout 1 >nul 2>&1

%JAR_PATH% -x -v -f "%ARCHIVE_PATH%"
if errorLevel 1 ( exit /b 102 )
echo "unpacked archive"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% -w
if errorLevel 1 ( exit /b 1 )
echo "user data wiped"

timeout 5 >nul 2>&1

echo "=== Flash slot A"

%FASTBOOT_PATH% flash system_a system.img -S 522239K
if errorLevel 1 ( exit /b 2 )
echo "flashed system"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash boot_a boot.img
if errorLevel 1 ( exit /b 2 )
echo "flashed boot"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash vendor_a vendor.img -S 522239K
if errorLevel 1 ( exit /b 2 )
echo "flashed vendor"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash dtbo_a dtbo.img
if errorLevel 1 ( exit /b 2 )
echo "flashed dtbo"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash vbmeta_a vbmeta.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vbmeta"

timeout 1 >nul 2>&1

echo "=== Flash slot B"

%FASTBOOT_PATH% flash system_b system.img -S 522239K
if errorLevel 1 ( exit /b 2 )
echo "flashed system"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash boot_b boot.img
if errorLevel 1 ( exit /b 2 )
echo "flashed boot"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash vendor_b vendor.img -S 522239K
if errorLevel 1 ( exit /b 2 )
echo "flashed vendor"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash dtbo_b dtbo.img
if errorLevel 1 ( exit /b 2 )
echo "flashed dtbo"

timeout 1 >nul 2>&1

%FASTBOOT_PATH% flash vbmeta_b vbmeta.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vbmeta"

timeout 1 >nul 2>&1

exit /b 0