:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: TODO: test if a device go from "sideload" mode to recovery

:: Parameter
:: $1: DEVICE_ID (optionnal)

:: Return
:: - displayed: DEVICE_ID device detected

:: Exit status
:: - 0 : New device detected
:: - 1 : Error
:: - 101 : DEVICES_LOCKED_PATH missing

set DEVICE_ID="%1"
set ADB_FOLDER_PATH=%~2

:: check device_ID est définit

if not defined %DEVICE_ID (
  exit /b 101
)

set ADB_PATH="%ADB_FOLDER_PATH%adb"

echo "waiting for recovery"

%ADB_PATH% -s %DEVICE_ID% wait-for-recovery
if not errorlevel 1 (
 	echo "device found in recovery"
  	%ADB_PATH% -s %DEVICE_ID% shell "twrp mount system"
	if  errorlevel 1 (
		echo "twrp mount system failed"
		exit /b 1
	)
) else (
	echo "device not detected in recovery"
	exit /b 2
)

exit /b 0