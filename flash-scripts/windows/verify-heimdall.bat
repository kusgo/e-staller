:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.
:: $1: The folder where heimdall runnable is stored
:: Exit status
:: - 0 : OEM unlocked on device
:: - 10 : heimdall error
set HEIMDALL_FOLDER=%~1

set HEIMDALL_PATH="%HEIMDALL_FOLDER%heimdall"

"%HEIMDALL_FOLDER%wdi-simple.exe"

ping 127.0.0.1 -n 3 -w 10000 >NUL

%HEIMDALL_PATH% print-pit
if errorLevel 1 (
    EXIT /b 10
)
ping 127.0.0.1 -n 2 -w 5000 >NUL

exit /b 0