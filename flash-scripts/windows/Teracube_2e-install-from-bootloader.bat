:: Copyright (C) 2021 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID device id
:: $2: ARCHIVE_PATH path to archive
:: $3: fastboot folder path
:: $4: Java folder path

:: Exit status
:: - 0 : Device flashed successfully
:: - 1 : Generic error
:: - 10: Error unpacking system.img
:: - 11: Error wiping user data
:: - 12: Error flashing preloader
:: - 13: Error flashing lk
:: - 14: Error flashing md1img
:: - 15: Error flashing logo
:: - 16: Error flashing scp1
:: - 17: Error flashing scp2
:: - 18: Error flashing spmfw
:: - 19: Error flashing sspm_1
:: - 20: Error flashing sspm_2
:: - 21: Error flashing tee1
:: - 22: Error flashing tee2
:: - 23: Error flashing boot
:: - 24: Error flashing dtbo
:: - 25: Error flashing recovery
:: - 26: Error flashing vbmeta
:: - 27: Error flashing vbmeta_system
:: - 28: Error flashing vbmeta_vendor
:: - 29: Error flashing super
:: - 30: Error rebooting to system
:: - 101 : DEVICE_ID missing
:: - 102 : ARCHIVE_PATH missing
:: - 103 : FASTBOOT_FOLDER_PATH missing
:: - 104 : JAVA_FOLDER_PATH missing

set DEVICE_ID="%1"
set ARCHIVE_PATH=%~2
set FASTBOOT_FOLDER_PATH=%~3
set JAVA_FOLDER_PATH=%~4

if not defined %DEVICE_ID (
  exit /b 101
)

if not defined %ARCHIVE_PATH (
  exit /b 102
)

if not defined %FASTBOOT_FOLDER_PATH (
  exit /b 103
)

:: Check java folder has been provided
if not defined %JAVA_FOLDER_PATH (
  exit /b 104
)

set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"


set JAR_PATH="%JAVA_FOLDER_PATH%/bin/jar"

:: Build archive folder path
for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH=%%~dpa"
	echo %ARCHIVE_FOLDER_PATH%
)
:: unzip for system.img
cd "%ARCHIVE_FOLDER_PATH%"

%JAR_PATH% -x -v -f "%ARCHIVE_PATH%"
if errorLevel 1 ( exit /b 10 )

echo "unpacked archive"

ping 127.0.0.1 -n 1 -w 10000 >NUL

%FASTBOOT_PATH% erase userdata
if errorLevel 1 ( exit /b 11 )

echo "user data wiped"

ping 127.0.0.1 -n 5 -w 10000 >NUL

%FASTBOOT_PATH% -s %DEVICE_ID% flash preloader preloader_zirconia.bin
if errorLevel 1 ( exit /b 12 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed preloader"

%FASTBOOT_PATH% -s %DEVICE_ID% flash lk lk.img
if errorLevel 1 ( exit /b 13 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed lk"

%FASTBOOT_PATH% -s %DEVICE_ID% flash md1img md1img.img
if errorLevel 1 ( exit /b 14 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed md1img"

%FASTBOOT_PATH% -s %DEVICE_ID% flash logo logo.bin
if errorLevel 1 ( exit /b 15 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed logo"

%FASTBOOT_PATH% -s %DEVICE_ID% flash scp1 scp.img
if errorLevel 1 ( exit /b 16 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed scp1"

%FASTBOOT_PATH% -s %DEVICE_ID% flash scp2 scp.img
if errorLevel 1 ( exit /b 17 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed scp2"

%FASTBOOT_PATH% -s %DEVICE_ID% flash spmfw spmfw.img
if errorLevel 1 ( exit /b 18 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed spmfw"

%FASTBOOT_PATH% -s %DEVICE_ID% flash sspm_1 sspm.img
if errorLevel 1 ( exit /b 19 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed sspm_1"

%FASTBOOT_PATH% -s %DEVICE_ID% flash sspm_2 sspm.img
if errorLevel 1 ( exit /b 20 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed sspm_2"

%FASTBOOT_PATH% -s %DEVICE_ID% flash tee1 tee.img
if errorLevel 1 ( exit /b 21 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed tee1"

%FASTBOOT_PATH% -s %DEVICE_ID% flash tee2 tee.img
if errorLevel 1 ( exit /b 22 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed tee2"

%FASTBOOT_PATH% -s %DEVICE_ID% flash boot boot.img
if errorLevel 1 ( exit /b 23 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed boot"

%FASTBOOT_PATH% -s %DEVICE_ID% flash dtbo dtbo.img
if errorLevel 1 ( exit /b 24 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed dtbo"

%FASTBOOT_PATH% -s %DEVICE_ID% flash recovery recovery.img
if errorLevel 1 ( exit /b 25 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed recovery"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vbmeta vbmeta.img
if errorLevel 1 ( exit /b 26 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vbmeta"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vbmeta_system vbmeta_system.img
if errorLevel 1 ( exit /b 27 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vbmeta_system"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vbmeta_vendor vbmeta_vendor.img
if errorLevel 1 ( exit /b 28 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed vbmeta_vendor"

%FASTBOOT_PATH% -s %DEVICE_ID% flash super super.img
if errorLevel 1 ( exit /b 29 )

ping 127.0.0.1 -n 1 -w 10000 >NUL
echo "Flashed super"
