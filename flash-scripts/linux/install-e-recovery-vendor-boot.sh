#!/bin/bash

# Copyright (C) 2022-23 ECORP SAS - Author: Frank
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: TWRP_IMAGE_PATH need twrp path (${TWRP_FOLDER}/${TWRP})
# $2: The folder where heimdall runnable is stored

# Exit status
# - 0 : Recovery installed
# - 1 : No problems occurred (heimdall returns only 1 if an error occurs)
# - 101 : TWRP_IMAGE_PATH missing

TWRP_IMAGE_PATH=$1
FASTBOOT_FOLDER_PATH=$2
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"

echo "fastboot path: $FASTBOOT_PATH"

if [ -z "$TWRP_IMAGE_PATH" ]
then
	echo "TWRP Image path is empty"
  	exit 101
fi

"$FASTBOOT_PATH" flash vendor_boot "$TWRP_IMAGE_PATH"