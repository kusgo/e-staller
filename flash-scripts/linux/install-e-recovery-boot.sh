#!/bin/bash

# Copyright (C) 2022 ECORP SAS - Author: Frank Preel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: FASTBOOT_PATH 
# $2: E_IMAGE_PATH need twrp path 
# Exit status
# - 0 : Recovery installed
# - 101 : FASTBOOT_PATH missing
# - 102 : E_IMAGE_PATH missing

#This script performs the same kind of job like `install-e-recovery` on the boot partion (rather than recovery patition)

FASTBOOT_PATH=$1
E_IMAGE_PATH=$2

if [ -z "$FASTBOOT_PATH" ]
then
	echo "Fastboot path is empty"
  	exit 101
fi

if [ -z "$E_IMAGE_PATH" ]
then
	echo "E Image path is empty"
  	exit 102
fi


# Build fastboot and adb commands
FASTBOOT_CMD=${FASTBOOT_PATH}"fastboot"
ADB_CMD=${FASTBOOT_PATH}"adb"


"$FASTBOOT_CMD" flash boot ${E_IMAGE_PATH}
