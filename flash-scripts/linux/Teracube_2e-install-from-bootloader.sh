#!/bin/bash

# Copyright (C) 2021 ECORP SAS - Author: Vincent Bourgmayer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: DEVICE_ID device id
# $2: ARCHIVE_PATH path to archive
# $3: fastboot folder path
# $4: Java folder path


# Exit status
# - 0 : Device flashed successfully
# - 1 : Generic error
# - 10: Error unpacking system.img
# - 11: Error wiping data
# - 12: Error flashing preloader
# - 13: Error flashing lk
# - 14: Error flashing md1img
# - 15: Error flashing logo
# - 16: Error flashing scp1
# - 17: Error flashing scp2
# - 18: Error flashing spmfw
# - 19: Error flashing sspm_1
# - 20: Error flashing sspm_2
# - 21: Error flashing tee1
# - 22: Error flashing tee2
# - 23: Error flashing boot
# - 24: Error flashing dtbo
# - 25: Error flashing recovery
# - 26: Error flashing vbmeta
# - 27: Error flashing vbmeta_system
# - 28: Error flashing vbmeta_vendor
# - 29: Error flashing super
# - 101 : DEVICE_ID missing
# - 102 : ARCHIVE_PATH missing
# - 103 : Fastboot folder path missing
# - 104 : Java folder path missing

DEVICE_ID=$1
ARCHIVE_PATH=$2
FASTBOOT_FOLDER_PATH=$3
JAVA_FOLDER_PATH=$4

# Check serial number has been provided
if [ -z "$DEVICE_ID" ]
then
  exit 101
fi

# check path to rom has been provided
if [ -z "$ARCHIVE_PATH" ]
then
  exit 102
fi

#check path to adb/fasboot has been provided
if [ -z "$FASTBOOT_FOLDER_PATH" ]
then
  exit 103
fi

# Check java folder has been provided
if [ -z "$JAVA_FOLDER_PATH" ]
then
  exit 104
fi


# Build fastboot path
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"

# Build java jar path
JAR_PATH=${JAVA_FOLDER_PATH}"/bin/jar"

# Build archive folder path
ARCHIVE_FOLDER_PATH=$(dirname "$ARCHIVE_PATH")"/"

# unzip for system.img
cd "$ARCHIVE_FOLDER_PATH" || exit 104

if ! "$JAR_PATH" -x -v -f "$ARCHIVE_PATH" ;
then
  exit 10
fi

echo "unpacked archive"

sleep 1

# Wipe user data
if ! "$FASTBOOT_PATH" erase userdata ;
then
  exit 11
fi

echo "user data wiped"
sleep 5

# Flash the device
if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash preloader preloader_zirconia.bin ; then
  exit 12
fi
sleep 1
echo "Flashed preloader"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash lk lk.img ; then
  exit 13
fi
sleep 1
echo "Flashed lk"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash md1img md1img.img ; then
  exit 14
fi
sleep 1
echo "Flashed md1img"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash logo logo.bin ; then
  exit 15
fi
sleep 1
echo "Flashed logo"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash scp1 scp.img ; then
  exit 16
fi
sleep 1
echo "Flashed scp1"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash scp2 scp.img ; then
  exit 17
fi
sleep 1
echo "Flashed scp2"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash spmfw spmfw.img ; then
  exit 18
fi
sleep 1
echo "Flashed spmfw"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash sspm_1 sspm.img ; then
  exit 19
fi
sleep 1
echo "Flashed sspm_1"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash sspm_2 sspm.img ; then
  exit 20
fi
sleep 1
echo "Flashed sspm_2"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash tee1 tee.img ; then
  exit 21
fi
sleep 1
echo "Flashed tee1"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash tee2 tee.img ; then
  exit 22
fi
sleep 1
echo "Flashed tee2"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash boot boot.img ; then
  exit 23
fi
sleep 1
echo "Flashed boot"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash dtbo dtbo.img ; then
  exit 24
fi
sleep 1
echo "Flashed dtbo"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash recovery recovery.img ; then
  exit 25
fi
sleep 1
echo "Flashed recovery"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash vbmeta vbmeta.img ; then
  exit 26
fi
sleep 1
echo "Flashed vbmeta"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash vbmeta_system vbmeta_system.img ; then
  exit 27
fi
sleep 1
echo "Flashed vbmeta_system"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash vbmeta_vendor vbmeta_vendor.img ; then
  exit 28
fi
sleep 1
echo "Flashed vbmeta_vendor"

if ! "$FASTBOOT_PATH" -s "$DEVICE_ID" flash super super.img ; then
  exit 29
fi
sleep 1
echo "Flashed super"
