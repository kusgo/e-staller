variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CONTAINER_IMAGE: registry.gitlab.e.foundation/e/devices/easy-installer
  CONTAINER_TAG: stable

stages:
- test
- prepare
- build
- publish

check-version:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  stage: test
  script:
    - ./version.sh check
  rules:
    - if: '$CI_COMMIT_TAG'
      when: always

build-docker:
  image: docker:19
  stage: prepare
  services:
    - docker:19-dind
  tags:
    - generic_privileged
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker pull $CONTAINER_IMAGE:$CONTAINER_TAG || true
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.e.foundation
    - docker build --cache-from $CONTAINER_IMAGE:$CONTAINER_TAG -t $CONTAINER_IMAGE:$CONTAINER_TAG .
    - docker push $CONTAINER_IMAGE:$CONTAINER_TAG
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - Dockerfile
      when: manual

build-aur-docker:
# build AUR docker image
# the image is different from the build-docker (which using docker:19) step as
# archlinux/archlinux docker image fails with
# error: failed to initialize alpm library:
  image: docker:20
  stage: prepare
  services:
    - docker:20-dind
  tags:
    - generic_privileged
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker pull $CONTAINER_IMAGE/aur:$CONTAINER_TAG || true
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - cd docker && docker build --cache-from $CONTAINER_IMAGE/aur:$CONTAINER_TAG -f Dockerfile.arch -t $CONTAINER_IMAGE/aur:$CONTAINER_TAG .
    - docker push $CONTAINER_IMAGE/aur:$CONTAINER_TAG
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - Dockerfile.arch
      when: manual

build-snap:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - snapcraft
    - ls
  artifacts:
    name: "easy-installer"
    paths:
      - easy-installer*.snap

build-linux-x64:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - ./gradlew dist
  artifacts:
    name: "easy-installer-$CI_COMMIT_TAG-linux_x64"
    paths:
      - build/distributions/easyInstaller-linux-x64.zip
  rules:
    - if: $CI_COMMIT_TAG

build-windows:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - ./gradlew jlink -Pkey="$PEPPER"
    - makensis windows-installer-mui.nsi
    - sha256sum Easy-installer-setup.exe > Easy-installer-setup.exe.sha256sum
  artifacts:
    name: "easy-installer-windows"
    paths:
      - Easy-installer-setup.exe
      - Easy-installer-setup.exe.sha256sum

build-macos:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  tags:
    - generic_privileged
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - ./gradlew jlink
    - cd Mac-build
    - ./mac_package.sh
    - sha256sum EasyInstaller.dmg > EasyInstaller.dmg.sha256sum
  artifacts:
    name: "easy-installer-mac"
    paths:
      - Mac-build/EasyInstaller.dmg
      - Mac-build/EasyInstaller.dmg.sha256sum

build-aur:
  image: $CONTAINER_IMAGE/aur:$CONTAINER_TAG
  stage: build
  before_script:
    - mkdir dist && cp -a pkg/arch/. dist
  script:
    - cd dist && makepkg -sifc --noconfirm
  artifacts:
    name: "easy-installer-aur"
    paths:
      - dist/*.pkg.tar.zst

publish-ubuntu:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  stage: publish
  needs: ["build-snap"]
  variables:
    RELEASE_TYPE: beta
  script:
    - echo ${SNAPCRAFT_LOGIN_FILE} | base64 -d > snapcraft.login && snapcraft login --with snapcraft.login
    - snapcraft upload *.snap --release $RELEASE_TYPE
    - snapcraft logout && rm snapcraft.login
  rules:
    - if: '$CI_COMMIT_TAG'
      when: manual


publish-macos:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  stage: publish
  needs: ["build-macos"]
  variables:
    RELEASE_TYPE: beta
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - "rsync -avz Mac-build/EasyInstaller.dmg* $PUBLISH_USER@$PUBLISH_URL:$PUBLISH_DEST"
    - ssh $PUBLISH_USER@$PUBLISH_URL "mv $PUBLISH_DEST/* $RELEASE_DEST/"
  rules:
    - if: '$CI_COMMIT_TAG'
      when: manual

publish-windows:
  image: registry.gitlab.e.foundation/e/devices/easy-installer:stable
  stage: publish
  needs: ["build-windows"]
  variables:
    RELEASE_TYPE: beta
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - "rsync -avz Easy-installer-setup.exe* $PUBLISH_USER@$PUBLISH_URL:$PUBLISH_DEST"
    - ssh $PUBLISH_USER@$PUBLISH_URL "mv $PUBLISH_DEST/* $RELEASE_DEST/"
  rules:
    - if: '$CI_COMMIT_TAG'
      when: manual

.publish-aur:
  image: $CONTAINER_IMAGE/aur:$CONTAINER_TAG
  stage: publish
  needs: ["build-aur"]
  variables:
    RELEASE_TYPE: beta
  before_script:
    # store the private key into the agent's memory
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY_ED" | tr -d '\r' | ssh-add - > /dev/null
    # add 'aur' to SSH configure
    - install -m '700' -d ~/.ssh
    - echo -e "Host aur aur.archlinux.org\n  User aur\n  Hostname aur.archlinux.org" > ~/.ssh/config
    - ssh-keyscan aur.archlinux.org > ~/.ssh/known_hosts
    # config git
    - git config --global user.email "$PUBLISH_USER_EMAIL"
    - git config --global user.name "$PUBLISH_USER"
  script:
    # Clone AUR repo
    - git clone ssh://aur@aur.archlinux.org/$CI_PROJECT_NAME.git
    # Copy package file from source to AUR clone
    - cp -a pkg/arch/. $CI_PROJECT_NAME/
    # Generate add, commit and push
    - |
      cd $CI_PROJECT_NAME/ &&
      makepkg --printsrcinfo > .SRCINFO &&
      git add . &&
      git commit -m "$CI_COMMIT_TAG" &&
      git push

publish-aur:
  extends:
    - .publish-aur
  environment:
    name: aur-repo
  rules:
    - if: '$CI_COMMIT_TAG'
      when: manual

publish-linux-x64:
  image: curlimages/curl:latest
  stage: publish
  needs: ["build-linux-x64"]
  variables:
    PKG_NAME: easy-installer-$CI_COMMIT_TAG-linux_x64.zip
  script:
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${CI_PROJECT_DIR}/build/distributions/easyInstaller-linux-x64.zip" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/easy-installer/$CI_COMMIT_TAG/$PKG_NAME"'
  rules:
    - if: '$CI_COMMIT_TAG'
