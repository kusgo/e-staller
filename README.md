# Easy Installer

## Documentation
To run directly from source:
`./gradlew run -Dorg.gradle.java.home=buildSrc/linux/jdk-11.0.2/`
from root of the project.

- [How to build from source with Gradle](../../wikis/Build-with-Gradle)
- [How to support new Device](../../wikis/Support-new-device)
- [How translation works](../../wikis/update-translation)

- [Weblate](https://i18n.e.foundation/projects/e/easy-installer/)



## Where to get sources (files flashed)
- windows: `C:\Users\<your username>\AppData\Local\easy-installer\sources\`
- linux: `~/snap/easy-installer/common/sources/`


## Where to get log at runtime
- Windows: `C:\Users\<your username>\AppData\Local\easy-installer`
- linux : `~/snap/easy-installer/common/`

## Techno dependancy
- Java
- JavaFX
- FXML
- YAML
- CSS
- Snapcraft
- NSIS

## Dependancy:
- Java 11+
- JavaFX 13+
- Flash-lib
- Gradle 4.10+
    - org.openjfx.javafxplugin
    - https://badass-jlink-plugin.beryx.org/
- SnakeYaml 1.24+ 
- Git
- Gitlab
- logback
- slf4j
