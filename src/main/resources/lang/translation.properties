# Copyright 2019-2020 - ECORP SAS 

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#   @author Vincent Bourgmayer
#   @author Romain Hunault
#   @author Alexis Noetinger
#   @author Brittny Mendoza




#common view
all_lbl_notImplemented=(Not yet implemented)
all_lbl_tryAgain=Try again
all_btn_needHel=Need help
all_btn_continue=Continue
all_btn_donate=Donate

#warning
warn_android_12=Before following these instructions please ensure that the device is on the latest Android 12 firmware.

#oneplus
install_instr_e_recovery_oneplus_select_recovery=Installing the partition

#pixel5
install_pixel_advance=Select Advanced
install_pixel_reboot=Select Reboot to bootloader
install_pixel_next=Once the device has rebooted
install_instr_adb_advance=Select the item "Advanced"
install_instr_adb_sideload=Select the item "adb sideload"
install_instr_adb_swipe=Swipe to validate, the install process will start automatically
install_instr_adb_start=Finally, on the phone, press Start

# Welcome Scene
welcome_mTitle=Welcome to the /e/OS Installer!
welcome_lbl_body1=This program will enable you to install /e/OS on your phone in a few minutes. Don't worry, it is simple and easy. 
welcome_lbl_body2=Get your smartphone and a USB cable and we'll get you up and running in no time!
welcome_lbl_version=Version x.x.x

#Before Scene
before_mTitle=Before we begin
before_title_time=Free some time
before_lbl_time=The complete installation might take some time. Make sure you have around 1 hour ahead of you between the time required to download the files and the time required to install the OS
before_title_space=Make some space
before_lbl_space=You will need to download several files. Please make sure you have enough available space on your computer to download at least 6Gb without additional costs
before_title_battery=Charge your phone
before_lbl_battery=Your phone needs to be charged at least at 50% to install /e/OS
before_title_backup=Back up your data
before_lbl_backup=Your smartphone data will be erased so we recommend that you back up your data. We will completely wipe your smartphone to install the OS.
before_title_usb=Grab a working cable
before_lbl_usb=You will need a good quality USB to make sure your phone is well detected by your computer. If possible, use USB 3 port or more recent.
before_lbl_ready=Ready, click on "continue"
#connect
connect_mTitle=Connect your device
connect_lbl_1=Connect your phone to your computer via USB
connect_lbl_2=We will automatically detect your phone to install /e/OS. Automatic device detection will not work if 'Developer Mode' isn't enabled.
connect_lbl_3=In the next stages, we will help you activate 'Developer mode' if you haven't done it so far.

#devMode & debugADB
devMode_mTitle_main=Enable the Developer mode
devMode_mTitle=Enable the Developer mode (Part 1)
devMode_lbl=Please follow these steps:
devMode_main=Your phone has just been unlocked, it has restarted, skip all initialization steps
devMode_instr_settings=Open the 'Settings' menu
devMode_instr_build=Type 'Build' in the search bar
devMode_instr_tap7=Tap 7 times on 'Build number'
debugADB_mTitle=Enable the Developer mode (Part 2)
debugADB_instr_settings=Open the 'Settings' menu again
debugADB_instr_search=Type 'Debug' in search bar
debugADB_instr_androidDebug=Tap on 'USB debugging' (on some devices it is called 'Android Debugging')
debugADB_instr_debugOn=Switch on 'Android Debug' / 'USB Debug'
debugADB_instr_tapeOK=Tap 'Ok' on pop up to confirm Allow USB debugging
debugADB_instr_acceptCertificate=if asked, Tick the checkbox and validate the message
enableMTP_mTitle=Enable 'USB file transfer'
enableMTP_instr_settings=Open 'developer options' in 'Settings'
enableMTP_instr_scrollToUSBConfig=Scroll down and select 'USB Configuration'
enableMTP_instr_selectMTP=Select 'File Transfer' (on some devices it is called 'MTP mode (media transfer protocol)')
removeAccounts_mTitle=Remove registered accounts
removeAccounts_instr_openAccounts=Open "Accounts" menu
removeAccounts_instr_selectAccounts=Select then remove every accounts

# Check windows driver installation
checkDriverInstall_mTitle=Drivers installation checking
checkDriverInstall_question=Did you install drivers for your devices?
checkDriverInstall_advice=If not, kindly follow the documentation available at :

#detect
detect_mTitle=Device detection
detect_lbl_detecting=Looking for your device...
detect_lbl_tryAnotherUsb=\nYou should try with another USB cable
detect_lbl_unknownDeviceFound=Unknown device found
detect_lbl_incompatibleDeviceFound=Your %s is not yet supported by /e/ OS
detect_lbl_compatibleDeviceFound=Your %s is compatible with /e/ OS
detect_btn_tryAgain=Try again
detect_btn_tryWithAnotherDevice=Try with another device
detect_lbl_unauthorizedDeviceFound=You are almost there!\n Your phone has been detected but it does not allow your computer to communicate with it
detect_lbl_acceptComputerFingerprint=If you see, on your device, a message like on the picture below:\n Tick the "Always allow from this computer" checkbox and tap on "Allow" button\nthen click on "Try again" below
detect_lbl_redisplayAllowUsbDebugingMsg=If you don't see this kind of message: unplug then replug your device\n The message will show again
detect_lbl_tooManyDevicesDetected=More than one android device has been detected.\nUnplug every device you won't flash. 

# new translation
askAccount_string={{mustardpepper}}

#Download
download_mTitle=Downloading /e/OS and necessary files
download_lbl_download=We will download the required files to your computer
download_lbl_bePatient=This might take some time, please be patient.
download_lbl_complete=Download is complete, you are now ready to install /e/OS on your phone!
download_lbl_checkingIntegrity=Checking integrity
download_lbl_noInternet=No internet connexion
download_lbl_serverUnreachable=Can't connect to server
download_lbl_fileNotFound=Can't find the remote file
download_lbl_cantcheckIntegrity=Can't check integrity
download_lbl_localFileNotFound=Local file not found
download_lbl_downloadError=Error while downloading
download_lbl_connectionLost=Connection lost. Trying to reconnect...
download_lbl_fileAlreadyUptoDate=File is already here and up-to-date.

#install
##FP3
install_title_lockBootloader=Lock the bootloader
install_instr_readAllWarning=Read all instructions before to start
install_instr_followOfficialGuidanceAt=Follow official guidance at
install_instr_selectUnlockBootloader=Select "UNLOCK BOOTLOADER" with "Volume" button
install_instr_unlockBootloader=Confirm with "Power" button. After that the phone will reboot automatically
install_instr_unlockBootloader_already=If your phone is already unlocked, you don't have to do anything. The phone will then restart automatically
install_instr_selectLockBootloader=Select "LOCK BOOTLOADER" with "Volume" button
install_instr_lockBootloader=Confirm with "Power" button. After that the phone will reboot automatically
install_instr_bootWarning=On the boot warning screen, you have 5 seconds to press "Volume Down", to enter options menu.  
install_instr_rejoinBootloader=Immediately hold down the Vol. Down button to enter Fastboot Mode. You have around 3 secs to do so.
install_instr_ifYouMissedTimeout=If you missed it, don't panic! Restart in Fastboot mode : turn off device then keep pressing "Power" & "Volume Down".
##FP4
#stepTitles
stepTitle_lockBootloaderCritical=Lock Critical Partitions
#install_instructions
install_instr_prepareFastboot_FP4=When the phone is completely off, press and hold the "Volume Down" button and reconnect the USB cable. 
install_instr_showFastboot=Wait until the device shows up into fastboot mode.
install_instr_ifYouMissedTimeout_FP4=If you missed it, dont't panic! Restart your device again into the fastboot mode:\nPull off the USB cable and turn off your device\nKeeping "Volume Down" button pressed, connect the USB cable again\nYou should now be in fastboot mode.
script_error_store_rom_info_1=Cannot get security patch information.
stepTitle_checkLock=Checking the lock
install_instr_lockfailed=if you see this message, it means that the bootloader has not been locked. In this case you will see a screen that the bootloader is not locked every time you start the device. You can ignore the message displayed at the startup of the phone and start it normally. 
stepTitle_checkLock_mac_bootloader=Restart in bootloader mode
install_instr_mac_bootloader=Using only the menu on the phone, select "Restart Bootloader" with the volume keys and confirm with "Power". 
install_instr_mac_bootloader_1=The phone will reboot and should proceed to the next step "Installation of /e/". If this is not the case, you must redo the operation below once again.

#
## General
install_title_Log=Log
install_btn_sendLog=Send to support
install_btn_sendLogAgain=Failure. Send again
install_btn_sendLogSuccess=Log sent
install_instr_turnOff=Turn off the phone
install_instr_turnOffAgain=Turn off the phone again
install_instr_startDownload=Keep pressing simultaneously "Power" & "Home" & "Volume Down" until a blue screen appear to access Download Mode
install_instr_startFastboot=Keep pressing simultaneously "Power" & "Volume Down" until a screen with green "START" appears to access Fastboot Mode
install_instr_startFastbootFromOptions=From options menu use "Volume Up/Down" to select "Fastboot" and "Power" to confirm
install_instr_acceptWarning=Accept warning by pressing on "Volume Up"
emerald_install_instr_accept_lock=Accept lock by pressing on "Volume Up"
install_instr_verifyHeimdall=Verify Heimdall
install_instr_oemUnlock=OEM Unlocking
install_instr_recoveryInstall=Recovery installation
install_instr_leaveDownload=Keep pressing simultaneously "Power" & "Home" & "Volume Down" until device turns off
install_instr_startRecovery=Keep pressing simultaneously "Power" & "Home" & "Volume Up" until 'teamwin' screen appears
install_instr_keepReadOnly=Tap on 'Keep Read Only'
install_instr_tapWipe=Tap on 'Wipe'
install_instr_tapFormatData=Tap on 'Format Data'
install_instr_writeYes=Write 'yes'
install_instr_validate=Validate
install_instr_backX3=Tap 'Back' 3 times
install_instr_tapReboot=Tap on 'Reboot'
install_instr_tapRebootRecovery=Tap on 'Recovery'
install_instr_doNotInstall=Tap on 'Do not install'
install_instr_swipeTwrp=If asked, swipe the arrowed bar at the bottom of the screen from left to right
install_instr_patchInstall=Patch installation
install_instr_vendorInstall=Vendors installation
install_instr_eosInstall=/e/ installation
install_instr_tapAdvancedWipe=Tap on 'Advanced Wipe'
install_instr_tickData=Tick 'Data'
install_instr_tapRepairChangeFs=Tap on 'Repair or Change File System'
install_instr_tapChangeFs=Tap on 'Change File System'
install_instr_tapExt3=Tap on 'EXT3'
install_instr_swipeForOk=Swipe the arrowed bar at the bottom of the screen from left to right  to confirm
install_instr_backX2=Tap on 'Back' 2 times
install_instr_resizeFs=Tap on 'Resize file System'
install_instr_tapRebootSystem=Tap on 'Reboot System'
install_instr_tapRebootPowerOff=Tap on 'Power Off'
install_instr_openSettings=Open 'Settings'
install_instr_openDevOptions=Open 'Developer options'
install_instr_enableOEMUnlock=enable 'OEM unlock'
install_instr_acceptFactoryReset=You'll have  to keep pressing "Power"  & "Bixby" & "Volume Down"   until you reach "Download mode" once your device is off. When you're ready, accept Factory Reset. 
install_instr_startDl_pressPowerBixbyVolDown=Keep pressing simultaneously "Power" & "Bixby" & "Volume Down" until a blue screen appear to access Download Mode
install_instr_startRec_pressPowerBixbyVolUp=Keep pressing simultaneously "Power" & "Bixby" & "Volume Up" until 'teamwin' screen appears
install_instr_startRec_pressPowerBixbyVolUp_e_reco=Keep pressing simultaneously "Power" & "Bixby" & "Volume Up" until 'E Revovery' screen appears. (Don't give up when the screen goes black)
install_instr_leaveDl_pressPowerBixbyVolDown=Keep pressing simultaneously "Power" & "Bixby" & "Volume Down" until device turns off
install_instr_update_stockrom=Update your device to the latest version
install_instr_connectTowifi=Connect your device to Wi-fi
install_instr_GS290_accessSystemSettings=Open Settings and go to "System"
install_instr_GS290_accessSystemUpdate=Open "System update"
install_instr_continueIfDeviceUpToDate=Once your device is up-to-date, you can click on continue. Note that you must have the latest Android 10 version installed.
install_instr_searchOEM=Tap on the search icon. Type "OEM" to search "OEM unlocking" option
install_instr_enableOEMUnlocking=Enable "OEM unlocking"
install_instr_acceptOEMUnlockWarning=Accept the warning message by tapping on "enable"
install_instr_onceDoneThenContinue=Once OEM unlocking is enabled, you can click on continue
install_instr_waitInstallStartAuto=Please wait, installation will start automatically
install_instr_onceDeviceRebootThenContinue=Your device will reboot automatically. Once it's done, you can click on continue
install_instr_rebootingOnBootloader=Your device will reboot automatically on bootloader mode
install_instr_pressVolUpToAcceptOEMUnlocking=Press "Volume up" to accept OEM unlocking
install_instr_pressVolDownToAcceptOEMUnlocking=Press "Volume down" to accept OEM unlocking
install_instr_unlockingOem=Easy-installer is unlocking OEM
install_instr_waitFastbootmodeDetected=The next step will start automatically once your device is detected in fastboot mode. If it takes longer than 30 seconds, please check our FAQ by clicking on the "Need help" button.
install_instr_choose_e_recovery_select=On the device by using Up or Down volume keys, select "Recovery Mode" option.
install_instr_choose_e_recovery_select_details=The selection is made by using the volume keys of the phone.
install_instr_choose_e_recovery_validate=Validate with Power
install_instr_choose_e_recovery_validate_wait_for_result=After a while the device will reboot in recovery mode.
install_instr_e_recovery_apply_update=Select "Apply update"
install_instr_e_recovery_apply_update_from_adb=Select "Apply update from ADB"
install_instr_e_recovery_apply_update_from_adb_wait_for_result=The installation is in progress, be patient
install_instr_e_recovery_apply_update_from_adb_accept_sign_fail=A screen that says Signature verification failed, this is expected, please accept this warning.
install_instr_e_recovery_factory_reset=Select "Factory reset"
install_instr_e_recovery_factory_reset_format_data=Select "Format data/factory reset"
install_instr_e_recovery_factory_reset_cache=Select "Format cache partition"
install_instr_e_recovery_factory_reset_system=Select "Format system partition"
install_instr_e_recovery_factory_reset_format_data_validate=Accept the warning by selecting "Format data"
install_instr_e_recovery_factory_reset_validate=Accept the warning by selecting "Yes"
install_instr_e_recovery_back=Select "Back", on the top left corner
install_instr_e_recovery_reboot=Select "Reboot system now"
install_instr_openSettingsThenDevOptions=Open "Settings" then "Developer options"
install_instr_disableAutoUpdateSystem=Disable "Auto update system"
install_instr_openSoftwareUpdate=Get back to "Settings" and open "Software update"
install_instr_disableAutomaticDownload=Disable "Auto download over Wi-Fi"
install_instr_clickDlAndInstall=Tap on "Download and install" and wait until you get result (a failure is OK)
install_instr_rebootDeviceThenContinue=Restart your device then click on continue
install_instr_waitRecoveryInstallStartAuto=Please wait, Installation of the /e/ recovery will start automatically
install_instr_leaveBootloader_holdPower=Hold "Power" button pressed until the phone turn off (~12 seconds)
install_instr_startRec_holdPowerVolUp=Hold "Power" and "Volume up" pressed until a small menu appear on your phone (~2 seconds)
install_instr_selectRecovery_pressVolUp=Press "Volume up" to select Recovery
install_instr_validRecovery_pressVolDown=Press "Volume down" to start in Recovery
install_instr_waitRecoveryStarted=Wait Recovery mode started
install_instr_selectApplyUpdate_pressVol=Select "Apply update" then "Apply from ADB". Use "Volume up/down" to select and "Power" to validate.
install_instr_waitForInstall=Please wait, installation will begin
install_warning_doNotWaitToRestartRecovery=Attention! Please read instructions carefully. You must restart in recovery mode immediately after the phone is turned off. Otherwise the phone will restart on your previous OS and the recovery's installation will not be taken into account. You\u2019ll have to start all over again.
script_error_waitDownload_1=Can't detect device in "Download mode"
script_error_waitFastboot_1=Can't detect device in "fastboot mode"
script_error_oemUnlock_10=Can't allow custom OS installation on your device
script_error_installRecovery_101=Can't install TWRP
script_error_waitRecovery_1=Can't mount the "system" folder
script_error_waitRecovery_101=No device's serial number provided
script_error_waitRecovery_102=Error while waiting for device to start in recovery
script_error_waitSideload_101=Error while waiting for device to start in sideload
script_error_installFromRecovery_1=Can't process the installation
script_error_installFromRecovery_2=Can't push the required file on the device
script_error_installFromRecovery_3=An error happened during the installation
script_error_installFromRecovery_101=No device's serial number provided
script_error_installFromRecovery_102=Can't locate the required file
script_error_installFromFastboot_1=Could not wipe user data
script_error_installFromFastboot_2=Flashing of one partition failed
script_error_installFromFastboot_3=Could not lock the bootloader
script_error_installFromFastboot_4=Setting boot slot has failed
script_error_installFromFastboot_5=Flashing of one critical partition failed
script_error_installFromFastboot_101=No /e/ install archive provided
script_error_installFromFastboot_102=Could not unpack /e/ install archive
script_error_installFromSideload=Could not install /e/ from eRecovery
script_error_waitReboot_10=No device's serial number provided
script_error_waitReboot_101=Can't run instruction on the device
script_error_waitRebootFromFastboot_101=Can't run instruction on the device
script_error_serialNumber_missing=No device's serial number provided
script_error_fastboot_path_missing=No fastboot tool path provided
script_error_fastboot_flashingUnlock_failed=Could not unlock flashing
script_error_fastboot_flashingLock_failed=Could not lock flashing
script_error_unknown=The installation encounter an error
script_error_cantRebootBootloader=Failed to reboot into bootloader
script_error_cantUnpackSources=Failed to unpack /e/ sources
script_error_cantWipeData=Failed to wipe data
script_error_cantActivePartition=Failed to active partition
script_error_cantFlashBoot=Failed to flash Boot partition
script_error_cantFlashDtbo=Failed to flash dtbo partition
script_error_cantFlashRecovery=Failed to flash Recovery
script_error_cantFlashLogo=Failed to flash logo partition
script_error_cantFlashMd1dsp=Failed to flash md1dsp partition
script_error_cantFlashMd1img=Failed to flash md1img partition
script_error_cantFlashSpmfw=Failed to flash spmfw partition
script_error_cantFlashLk=Failed to flash lk partition
script_error_cantFlashLk2=Failed to flash lk2 partition
script_error_cantFlashSspm_1=Failed to flash sspm_1 partition
script_error_cantFlashSspm_2=Failed to flash sspm_2 partition
script_error_cantFlashTee1=Failed to flash tee1 partition
script_error_cantFlashTee2=Failed to flash tee2 partition
script_error_cantFlashPreloader=Failed to flash preloader partition
script_error_cantFlashVbmeta=Failed to flash vb meta partition
script_error_cantFlashVbmeta_system=Failed to flash vb meta system partition
script_error_cantFlashVBmeta_vendor=Failed to flash vb Meta vendor partition
script_error_cantFlashSystem=Failed to flash system partition
script_error_cantFlashproduct=Failed to flash product partition 
script_error_cantFlashVendor=Failed to flash vendor partition
script_error_cantFlashGz_a=Failed to flash gz_a partition
script_error_cantFlashLk_a=Failed to flash lk_a partition
script_error_cantFlashScp_a=Failed to flash scp_a partition
script_error_cantFlashMd1img_a=Failed to flash md1img_a partition
script_error_cantFlashSpmfw_a=Failed to flash spmfw_a partition
script_error_cantFlashSspm_a=Failed to flash sspm_a partition
script_error_cantFlashTee_a=Failed to flash tee_a partition
script_error_cantFlashBoot_a=Failed to flash boot_a partition
script_error_cantFlashDtbo_a=Failed to flash dtbo_a partition
script_error_cantFlashVbmeta_a=Failed to flash vbmeta_a partition
script_error_cantFlashSuper=Failed to flash super partition
script_error_cantrebootFromFasboot=Failed to reboot from fastboot
script_error_cantRebootToFastboot=Failed to reboot into fastboot mode
java_error_unknow=The installation encounter an internal error
flash_process_cancelled=The installation process has been cancelled

#oneplus
install_instr_e_recovery_oneplus_copy_partition=Partition installation

#eAccount
eAccount_mTitle=Create your e.email account
eAccount_lbl_incitation=Your e.email account is at the center of the /e/OS ecosystem.
eAccount_title_createAccount=Create my e.email personal account
eAccount_lbl_exampleField=name@example.com
eAccount_lbl_invalidMail=Please enter a valid email address to receive your registration link
eAccount_title_alreadyAccount=I already have an e.email account or I'm not interested
eAccount_lbl_alreadyAccount=Click on continue
eAccount_lbl_dontuseEmail=Do not use @e.email address
eAccount_btn_checkMail=OK
eAccount_lbl_invitationSent=Nice! you'll receive your invitation e-mail. Don't forget to check your SPAM folder.
eAccount_lbl_mailAlreadyUsed=Oops! This adress has already been used.
eAccount_lbl_unsupportedFormat=Your email has an unsupported format
eAccount_lbl_emailNotSent=Your e-mail hasn't been sent
eAccount_lbl_onlineFailure=Online checking of your e-mail failed. Please try again
eAccount_lbl_tryLater=There was an issue. Please try later
eAccount_lbl_contactSupport=hm..There was an issue, please contact support!
eAccount_lbl_processing=Processing...

#result
result_title_installOver=Installation over
result_lbl_msg1=Installation is over. While your phone is booting, your should see the logo below.
result_lbl_takeTime=This takes time, please be patient.

#congrats
congrats_mTitle=Congrats! You have just freed your phone!
congrats_title_success=The installation process is now complete.
congrats_lbl_setup=Follow the welcome screen instructions on your phone to set up your device.
congrats_lbl_recover=You can now add your files back on your phone

#Feedbacks
feedback_mTitle=Help us to improve the tool
feedback_lbl_yourFeel=How satisfied are you with this software ease's of use ?
feedback_lbl_comment=Do you have any thoughts on how to improve this software ?
feedback_lbl_thanks=Thanks
feedback_lbl_sendPrecision=No identifiable information will be sent
feedback_btn_leave=Leave
feedback_btn_send=Share anonymously with /e/ developers
feedback_btn_sendTryAgain=Failure. Try again to send feedback

#credits
# @TODO

# old translation
# From MainApplication.java
appTitle=/e/OS Easy Installer - 

### Flash 
installationTitle=Installation

# Title
stepTitle1On7=Connect device and start Download mode
stepTitleOemUnlock=Unlock OEM
stepTitle_criticalUnlock=Critical Unlock
stepTitleCheckSPL=Checking Security Patch Level
stepTitle3On7=Restart device in Download mode
stepTitle_unlockBootloader=Unlock Bootloader and restart device in Fastboot mode
stepTitle_unlockBootloaderCritical=Critical Unlock Bootloader and restart device in Fastboot mode
stepTitle_lockBootloader=Lock Bootloader and restart device in Fastboot mode
stepTitle4On7=Recovery installation
stepTitle5On7=Restart device in Recovery mode
stepTitle6On7=/e/ Installation
stepTitle7On7=Resize Data partition
stepTitle_verifyHeimdall=Verify Heimdall
stepTitle_oemUnlock=Unlock OEM
stepTitle_installRecovery=Recovery Installation
stepTitle_startRecovery=Start in Recovery mode
stepTitle_formatDataTurnOff=Format data and turn off
stepTitle_restartRecovery=Restart in Recovery mode
stepTitle_installOS=/e/ Installation
stepTitle_resizeDataPartition=Resize Data partition
stepTitle_StartInFastbootFP3=Start device in Fastboot mode
stepTitle_checkDeviceUptodate=Check device is up-to-date
stepTitle_enableOemUnlock=Enable OEM unlocking
stepTitle_beforeInstallation=Before installation
stepTitle_rebootDevice=Reboot device
stepTitle_rebootBootloader=Rebooting in bootloader mode
stepTitle_installation=Factory reset and installation
