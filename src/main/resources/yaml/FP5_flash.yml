## Copyright 2021-2022 - ECORP SAS

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.
## Author: Vincent Bourgmayer, Frank
---
stepsCount: 14
steps:
    f0:
        type: enableOemUnlock
        stepNumber: 1
        nextStepKey: f1
    f1:
        type: load
        stepNumber: 2
        nextStepKey: f2
        titleKey: stepTitleCheckSPL
        instructions:
            - install_instr_oemUnlock
        averageTime: 1
        script: store-rom-info_FP5
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
          archive_path: ${ARCHIVE_PATH}
          device_model: ${DEVICE_MODEL}
        okCodes:
          0: ~
        koCodes:
          1: script_error_store_rom_info_1
    f2:
        type: custom-executable
        stepNumber: 3
        nextStepKey: f3
        titleKey: stepTitle_StartInFastbootFP3
        titleIcon: icon-download.png
        instructions:
            - install_instr_turnOff
            - install_instr_prepareFastboot_FP4
            - install_instr_showFastboot
            - install_instr_waitFastbootmodeDetected
        script: wait-fastboot
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
        okCodes:
          0: ~
        koCodes:
          1: script_error_waitFastboot_1
    f3:
        type: load
        stepNumber: 4
        nextStepKey: f4
        titleKey: stepTitleOemUnlock
        instructions:
            - install_instr_oemUnlock
        averageTime: 5
        script: fp4_oem-unlock
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
        okCodes:
          0: ~
        koCodes:
          10: script_error_oemUnlock_10
    f4:
        type: custom-executable
        stepNumber: 5
        nextStepKey: f5
        titleKey: stepTitle_unlockBootloader
        titleIconName: icon-download.png
        instructions:
            - install_instr_readAllWarning
            - install_instr_selectUnlockBootloader
            - install_instr_unlockBootloader
            - install_instr_rejoinBootloader
            - install_instr_ifYouMissedTimeout_FP4
        script: wait-fastboot-unlocked
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
        okCodes:
          0: ~
        koCodes:
          1: script_error_waitFastboot_1
    f5:
        type: load
        stepNumber: 6
        nextStepKey: f6
        titleKey: stepTitle_criticalUnlock
        instructions:
            - install_instr_oemUnlock
        averageTime: 4
        script: fp4_oem-unlock-critical
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
        okCodes:
          0: ~
        koCodes:
          10: script_error_oemUnlock_10
    f6:
        type: custom-executable
        stepNumber: 7
        nextStepKey: f7
        titleKey: stepTitle_unlockBootloaderCritical
        titleIconName: icon-download.png
        instructions:
            - install_instr_readAllWarning
            - install_instr_selectUnlockBootloader
            - install_instr_unlockBootloader
            - install_instr_rejoinBootloader
            - install_instr_ifYouMissedTimeout_FP4
        script: wait-fastboot-unlocked-critical
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
        okCodes:
          0: ~
        koCodes:
          1: script_error_waitFastboot_1
    f7:
        type: custom-executable
        stepNumber: 8
        nextStepKey: f8
        titleKey: stepTitle_checkLock_mac_bootloader
        instructions:
            - install_instr_mac_bootloader
            - install_instr_mac_bootloader_1
        script: check_macos
        parameters:
          archive_path: ${ARCHIVE_PATH}
          fastboot_folder_path: ${ADB_FOLDER_PATH}
        okCodes:
          0: ~
        koCodes:
          101: script_error_installFromFastboot_101
    f8:
        type: load
        stepNumber: 9
        nextStepKey: f9
        titleKey: stepTitle6On7
        instructions:
            - install_instr_eosInstall
        averageTime: 300
        script: fp5_install-from-fastboot
        parameters:
          archive_path: ${ARCHIVE_PATH}
          fastboot_folder_path: ${ADB_FOLDER_PATH}
          java_folder_path: ${JAVA_FOLDER_PATH}
        okCodes:
          0: ~
        koCodes:
          1: script_error_installFromFastboot_1
          2: script_error_installFromFastboot_2
          4: script_error_installFromFastboot_4
          5: script_error_installFromFastboot_5
          101: script_error_installFromFastboot_101
          102: script_error_installFromFastboot_102
    f9:
        type: askAccount
        stepNumber: 10
        nextStepKey: f10
    f10:
        type: load
        stepNumber: 11
        nextStepKey: f11
        titleKey: install_title_lockBootloader
        instructions:
            - install_instr_oemUnlock
        averageTime: 5
        script: fp4_lock
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
          archive_path: ${ARCHIVE_PATH}
          device_model: ${DEVICE_MODEL}
        okCodes:
          0: ~
        koCodes:
          101: script_error_waitRebootFromFastboot_101
          102: script_error_installFromFastboot_3
    f11:
        type: custom-executable
        stepNumber: 12
        nextStepKey: f12
        titleKey: stepTitle_lockBootloader
        titleIconName: icon-download.png
        instructions:
            - install_instr_readAllWarning
            - install_instr_selectLockBootloader
            - install_instr_lockBootloader
            - install_instr_rejoinBootloader
            - install_instr_ifYouMissedTimeout_FP4
        script: wait-fastboot-locked
        parameters:
          fastboot_folder_path: ${ADB_FOLDER_PATH}
          archive_path: ${ARCHIVE_PATH}
          device_model: ${DEVICE_MODEL}
        okCodes:
          0: ~
        koCodes:
          1: script_error_waitFastboot_1
    f12:
        type: custom-executable
        stepNumber: 13
        nextStepKey: f13
        titleKey: stepTitle_lockBootloaderCritical
        titleIconName: icon-download.png
        instructions:
            - install_instr_selectLockBootloader
            - install_instr_lockBootloader
        script: fp4_lock_critical
        parameters:
            fastboot_folder_path: ${ADB_FOLDER_PATH}
            archive_path: ${ARCHIVE_PATH}
            device_model: ${DEVICE_MODEL}
        okCodes:
          0: ~
        koCodes:
          1: script_error_unknown
          101: script_error_installFromFastboot_3
    f13:
        type: custom-executable
        stepNumber: 14
        nextStepKey: end
        titleKey: stepTitle_checkLock
        titleIconName: icon-download.png
        instructions:
            - install_instr_lockfailed
        script: fp4_check_bootloader
        parameters:
            fastboot_folder_path: ${ADB_FOLDER_PATH}
            archive_path: ${ARCHIVE_PATH}
            device_model: ${DEVICE_MODEL}
        okCodes:
          0: ~
        koCodes:
          1: script_error_unknown
          101: script_error_installFromFastboot_3
