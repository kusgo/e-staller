/*
 * Copyright 2019-2020 - ECORP SAS

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.logger;

import ch.qos.logback.core.PropertyDefinerBase;
import ecorp.easy.installer.AppConstants;
import java.util.UUID;

/**
 *
 * @author vincent
 */
public class LogPathPropertyDefiner extends PropertyDefinerBase{
    private static final String logFilePath = AppConstants.getWritableFolder()+UUID.randomUUID()+".log";
    
    
    /**
     * This is access for logback.xml
     * @return 
     */
    @Override
    public String getPropertyValue() {
        return logFilePath;
    }
    
    /**
     * This is access for FlashSceneController.java
     * @return 
     */
    public static String getLogFilePath(){
        return logFilePath;
    }
}
