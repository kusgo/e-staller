/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ecorp.easy.installer.controllers;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.logger.GUIAppender;
import ecorp.easy.installer.logger.LogPathPropertyDefiner;
import ecorp.easy.installer.tasks.UploadToEcloudTask;
import ecorp.easy.installer.utils.UiUtils;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.TextFlow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the controller in charge of UI element to display log
 * @author vincent Bourgmayer
 */
public class LogController implements Initializable{

    private final static Logger logger = LoggerFactory.getLogger(LogController.class);
    
    //element relatives to log display
    @FXML ScrollPane logScrollPane;
    @FXML TextFlow logFlow;
    @FXML Button showHideLogBtn;
    @FXML Button sendLogBtn;
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        logFlow.heightProperty().addListener(observable -> logScrollPane.setVvalue(1.0)); 
        
        logScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    }
    
    
    /**
     * enable/disable log displaying
     */
    public void showHideLog(){
        ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        GUIAppender appender = (GUIAppender) rootLogger.getAppender("GUI");

        final String text = showHideLogBtn.getText();
        if(text.equals(">")){
            showHideLogBtn.setText("V");
            UiUtils.hideNode(logScrollPane);
            logFlow.getChildren().clear();
            appender.setContainer(null);
            logScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        }else{
            showHideLogBtn.setText(">");
            UiUtils.showNode(logScrollPane);
            logFlow.getChildren().addAll(appender.getLogsList());
            appender.setContainer(logFlow);
            logScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        }
    }
    
    /**
     * Send log to support, in special ecloud.global folder
     * @return 
     */
    public boolean sendLogToSupport(){
        //if (thread != null){
            String filePath = LogPathPropertyDefiner.getLogFilePath();
            if(filePath != null){

                UploadToEcloudTask uploadTask = new UploadToEcloudTask(AppConstants.LOG_STORAGE_URL, filePath);
                uploadTask.setOnSucceeded(eh -> {
                    if( (Boolean) eh.getSource().getValue() ){ //if success
                        sendLogBtn.setDisable(true);
                        //sendLogBtn.setText(i18n.getString("install_btn_sendLogSuccess"));
                        logger.info("sendLogToSupport(), sending log: success");
                    }
                    else{
                        //sendLogBtn.setText(i18n.getString("install_btn_sendLogAgain")); 
                        logger.warn("sendLogToSupport(), sending log: failure");
                    }
                });
                
                uploadTask.setOnFailed(eh->{
                    //sendLogBtn.setText(i18n.getString("install_btn_sendLogAgain")); 
                    logger.warn("sendLogToSupport(), sending log: failure, error = {}",eh.getSource().getException().toString() );
                });
                
                Thread uploadTaskThread = new Thread(uploadTask);
                uploadTaskThread.setDaemon(true);
                uploadTaskThread.start();
            }
        return false;
    }
    
    public void showSendLogBtn(){
        UiUtils.showNode(sendLogBtn);
    }
}