/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.stepControllers;


import ecorp.easy.installer.graphics.FlashGlobalProgressManager;
import ecorp.easy.installer.graphics.Instruction;
import ecorp.easy.installer.models.steps.ICustomStep;
import ecorp.easy.installer.utils.UiUtils;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author vincent Bourgmayer
 */
public class CustomStepController extends StepController<ICustomStep>{

    protected final ResourceBundle IMG_BUNDLE = ResourceBundle.getBundle("instructions.imageName",
        new Locale.Builder()
       .setRegion("en")
       .setLanguage("EN")
       .setVariant(parentController.getPhone().getInternalCode())
       .build());
    
    private int currentEmphasizedInstruction = 0; 
    private int instructionsCount;

    @FXML private VBox instructionsContainer;
    @FXML private Label titleLbl;
    @FXML private ImageView imgView;
    // progress bar's node of global Flash's process :
    @FXML HBox globalProgressIndicator;
    private FlashGlobalProgressManager globalProgressMgr;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb); 
        logger.debug("initialize customStep controller");
        
        parentController.setNextButtonVisible(true);
        titleLbl.setText(i18n.getString(step.getTitleKey() ));
        if(step.getTitleIconName() != null){
            titleLbl.setGraphic(new ImageView(UiUtils.loadImage(step.getTitleIconName())));
            titleLbl.setGraphicTextGap(18); //set spacing between icon and text
            titleLbl.setContentDisplay(ContentDisplay.LEFT); //set Icon to be displayed on left
        }
        
        final ArrayList<Instruction> instructions = loadInstructions();
        instructions.get(currentEmphasizedInstruction).emphasize();
        instructionsContainer.getChildren().addAll(instructions);
        displayInstructionImage(instructions.get(currentEmphasizedInstruction));
        
        instructionsCount = instructions.size();

        
        globalProgressMgr = new FlashGlobalProgressManager(parentController.getPhone().getFlashingProcess().getStepsCount());
        for(int i = 0;i < step.getStepNumber(); i++){
            globalProgressMgr.updateProgression();
        }
        globalProgressIndicator.getChildren().addAll(globalProgressMgr.getSegments());
    }
    
    
    @Override
    protected void onContinueClicked(){
        if(currentEmphasizedInstruction+1 == instructionsCount){
            UiUtils.buildFadeTransition(uiRoot, false);
            super.onStepEnd();
        }else{
            final Instruction currentInstruction = (Instruction) instructionsContainer
                    .getChildren().get(1+currentEmphasizedInstruction++);//the +1 is to ignore the title
            currentInstruction.trivialize();
            
            final Instruction nextInstruction = (Instruction) instructionsContainer
                    .getChildren().get(1+currentEmphasizedInstruction); //the +1 is to ignore the title
            nextInstruction.emphasize();
            
            displayInstructionImage(nextInstruction);
        }
    }
    
    /**
     * Create Instruction nodes with text key from step
     */
    private ArrayList<Instruction> loadInstructions(){
        final ArrayList<Instruction> result = new ArrayList<>();
        
        for(String key : step.getTextContentKeys()){
            String contentKey = "all_lbl_notImplemented"; //set default key
            System.out.println("translation key: "+key);
            if(i18n.containsKey(key)){
                contentKey = key;
            }
            result.add(new Instruction(key, i18n.getString(contentKey)));
        }
        return result;
    }
    
    
    /**
     * Display Image corresponding to the current Instruction
     * if there is one. In other way, it remove current image
     * @param instruction 
     */
    private void displayInstructionImage(Instruction instruction){
        if(IMG_BUNDLE.containsKey(instruction.getKey())){
            imgView.setManaged(true);
            imgView.setImage( 
                UiUtils.loadImage( IMG_BUNDLE.getString( instruction.getKey() ) ) );
        }else{
            imgView.setImage(null);
            imgView.setManaged(false);
        }
    }    
}