/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.stepControllers;

import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.steps.CommandExecutionResult;
import ecorp.easy.installer.models.steps.IExecutableStep;
import ecorp.easy.installer.tasks.CommandRunnerService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.concurrent.Task;

/**
 * @TODO - add a error and cancellation listener on the Service
 * @author vincent Bourgmayer
 */
public class ExecutableStepController extends StepController<IExecutableStep>{

    Task task; 
    CommandRunnerService backgroundService;
    
    @Override
    public void initialize(URL arg0, ResourceBundle resources) {
        super.initialize(arg0, resources); //To change body of generated methods, choose Tools | Templates.
    
        final Command cmd = step.getCommand();
        backgroundService = new CommandRunnerService(cmd);

        backgroundService.setOnSucceeded(eh->{
            CommandExecutionResult result = backgroundService.getValue();
            if(result.isSuccess())
                onStepEnd();
            else{
                String errorMsgKey = cmd.getErrorMsg(result.getExitCode());
                if (errorMsgKey == null || errorMsgKey.isEmpty()){
                    errorMsgKey = "script_error_unknown";
                }
                displayError(errorMsgKey);
            }

        });
        
        backgroundService.start();
    }
    
    
    protected void displayError(String errorMsgKey){
        //Show error UI and hide everything that should be hidden
        //like restart btn, etc.
        if(i18n.containsKey(errorMsgKey)){
            //Set text to a label
        }
    }
    
}
