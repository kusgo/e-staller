/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ecorp.easy.installer.controllers.stepControllers;

/**
 *
 * @author vincent
 */
public interface Stoppable {
    public void stop();
}
