/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.stepControllers;

import ecorp.easy.installer.controllers.LogController;
import static ecorp.easy.installer.controllers.stepControllers.StepController.parentController;
import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.steps.LoadStep;
import ecorp.easy.installer.tasks.CommandRunnerService;
import ecorp.easy.installer.graphics.FlashGlobalProgressManager;
import ecorp.easy.installer.models.steps.CommandExecutionResult;
import ecorp.easy.installer.models.steps.IStep;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 *
 * @author vincent Bourgmayer
 */
public class LoadStepController extends StepController<LoadStep> implements Stoppable{
    
    @FXML private ProgressBar taskProgressBar;
    @FXML private Label titleLbl;
    @FXML private VBox instructionsContainer;
    @FXML private ImageView imgView;
    @FXML private LogController logRootController; //the field name must be "<fx:id>Controller"
    
    private Timeline pbTimeline; //progressBar Timeline
    private CommandRunnerService service;
    
    // progress bar's node of global Flash's process :
    @FXML HBox globalProgressIndicator;
    private FlashGlobalProgressManager globalProgressMgr;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb); 

        parentController.setNextButtonVisible(false);
        
        instructionsContainer.setManaged(false);
        imgView.setManaged(false);
        /*if(step.getTextContentKeys().isEmpty()){
            instructionsContainer.setVisible(false);
        }*/
        
        
        if(i18n.containsKey(step.getTitleKey())){
            titleLbl.setText(i18n.getString(step.getTitleKey() ) );
        }
        
        
        globalProgressMgr = new FlashGlobalProgressManager(parentController.getPhone().getFlashingProcess().getStepsCount());
        for(int i = 0;i < step.getStepNumber(); i++){
            globalProgressMgr.updateProgression();
        }
        globalProgressIndicator.getChildren().addAll(globalProgressMgr.getSegments());
        
        /** Prepare ProgressBar animation **/
        final int averageTime = step.getAverageTime();
        if(averageTime > 0){
            pbTimeline = new Timeline(
                    new KeyFrame(Duration.millis(0), new KeyValue(taskProgressBar.progressProperty(), 0.0) ),
                    new KeyFrame(Duration.millis(averageTime*1000.0), new KeyValue(taskProgressBar.progressProperty(), 1.0))
                );
        }
        
        /** Prepare Command execution **/
        final Command cmd = step.getCommand();
        service = new CommandRunnerService(cmd);

        service.setOnSucceeded(eh->{
            pbTimeline.stop();
            CommandExecutionResult result = service.getValue();
            
            if(result.isSuccess()){
                onStepEnd();
            }else{
                String errorMsgKey = cmd.getErrorMsg(result.getExitCode());
                if (errorMsgKey == null || errorMsgKey.isEmpty()){
                    errorMsgKey = "script_error_unknown";
                }
                displayError(errorMsgKey);
            }

        });
        
        service.setOnRunning(eh->{
            taskProgressBar.setProgress(0.0); //Reset to prevent showing last value
            pbTimeline.playFromStart();
        });
        
        
        service.setOnCancelled(eh->{
            pbTimeline.stop();
        });
        
        service.start();
    }

    
    protected void displayError(String errorMsgKey){
        
        //parentController.setIsFlashed(false);
        parentController.setCurrentStepKey(IStep.LAST_STEP_KEY);
        parentController.resetNextButtonEventHandler();
        parentController.setNextButtonVisible(true);
        
        pbTimeline.stop();
        taskProgressBar.getStyleClass().add("errorBar");
        if(i18n.containsKey(errorMsgKey)){
            Label errorLbl = new Label(i18n.getString(errorMsgKey));
            ((VBox) uiRoot).getChildren().add(3, errorLbl);
        }else{
            logger.error("Missing translation for error key: {}", errorMsgKey);
        }
        
        logRootController.showSendLogBtn();
        
        final Button tryAgainBtn = new Button(i18n.getString("all_lbl_tryAgain"));
        tryAgainBtn.setOnMouseClicked(( MouseEvent event) -> {
            parentController.retryToFlash();
        });
        ((VBox)uiRoot).setAlignment(Pos.TOP_CENTER);
        ((VBox)uiRoot).getChildren().add(4,tryAgainBtn);

    }

    @Override
    public void stop() {
        logger.debug("LoadStepController.stop()");
        service.cancel();
    }
}
