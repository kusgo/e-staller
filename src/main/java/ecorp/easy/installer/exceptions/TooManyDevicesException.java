/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.exceptions;

/**
 * help: https://stackoverflow.com/questions/25863480/javafxs-task-seem-to-consume-exceptions-is-it-a-bug-or-a-feature
 * @author vincent Bourgmayer
 */
public class TooManyDevicesException extends Exception{
    int devicesAmount = 0;
    
    public TooManyDevicesException(int devicesAmount){
        this.devicesAmount = devicesAmount;
    }
}
