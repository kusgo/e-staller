/*
 * Copyright 2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.tasks;

import ecorp.easy.installer.models.Command;
import ecorp.easy.installer.models.steps.CommandExecutionResult;
import javafx.concurrent.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service in charge to run Command task
 * @author vincent Bourgmayer
 */
public class CommandRunnerService extends Service<CommandExecutionResult>{
    private final static Logger logger = LoggerFactory.getLogger(CommandRunnerService.class);
    private final Command cmd;
    
    public CommandRunnerService( Command cmd){
        this.cmd = cmd;
    }
    
    @Override
    protected CommandExecutionTask createTask() {
        CommandExecutionTask task = new CommandExecutionTask(cmd);
        return task;
    }
}
