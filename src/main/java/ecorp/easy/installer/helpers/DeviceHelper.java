/*
 * Copyright 2019-2020 - ECORP SAS

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.helpers;
import ecorp.easy.installer.models.steps.IStep;
import ecorp.easy.installer.models.Process;
import ecorp.easy.installer.utils.ConfigParser;
import static ecorp.easy.installer.utils.ConfigParser.parseSteps;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author Andre Lam
 * @author Vincent Bourgmayer
 * @author Ingo
 */
public class DeviceHelper {
    private final static Logger logger = LoggerFactory.getLogger(DeviceHelper.class);
    private final static String YAML_FOLDER_PATH = "/yaml/";
    
    private static final HashMap<String, String> map  = new HashMap<String, String>() {{
        // Format e95acf4b device usb:3-1.4 product:OnePlus8Pro_EEA model:IN2023 device:OnePlus8Pro transport_id:5
        // Keep model as identifier -> getDeviceInternalcode
        put("hero2lte", "0001");
        put("herolte",  "0002");
        put("star2lte", "0003");
        put("starlte",  "0004");
        //put("zeroflte", "0005"); Device not supported anymore
        put("dream2lte", "0006");
        put("dreamlte", "0007");
        put("FP3",      "0008");
        put("GS290",    "0009");
        put("Teracube_2e", "0011");
        put("emerald", "0012"); //Teracube A10
        put("OnePlus7T", "0013"); 
        put("OnePlus8", "0014"); 
        put("sunfish", "0015");
        put("flame", "0016");
        put("coral", "0017");
        put("keyword", "0018"); //OnePlus7 Pro
		put("FP4",      "0019");
        put("OnePlus8Pro", "0021");
        put("OnePlusNord", "0022"); //avicii
		put("redfin", "0023");
    }};

    /**
     * Return internal code for a given device
     * @param key the ADB device code's name (example: Samsung galaxy S7 => herolte)
     * @return can return null if no key matches
     */
    public static String getDeviceInternalcode(String key){
        return map.get(key);
    }

    /**
     * Returns true if the device is blacklisted. A blaclisted device embeds flashs scripts but 
     * is not detected by the Easy installer (use case  Murena ROM has an issue we nned to wait for the next ROM release
     *  but developping procces is allowed).
     * @param key the ADB device code's name (example: Samsung galaxy S7 => herolte comma separated)
     * @return can return null if no key matches
     */
    public static boolean isDeviceBlacklisted(String key){
        List<String> blaclisted = Arrays.asList();
        return blaclisted.contains(key);
    }
    
    
    /**
     * Get data relatives to sources to download from config file
     * @param adbDevice
     * @return
     * @throws IOException
     * @throws ParseException
     * @throws ClassCastException
     * 
     * todo: definitively need  more tests 
     */
    public static HashMap<String, String> getSourcesToDownload(String adbDevice) throws IOException,ParseException, ClassCastException{

        return ConfigParser.parseSourcesToDownload( (HashMap) loadYaml(adbDevice+"_fs.yml").get("sources") );
    }
    
    
    /**
     * Load a yaml file 
     * @param filename the filename (don't forget extension)
     * @return a Map instance with yaml parsed content, or null
     * @throws IOException yaml file access give IO Error
     */
    private static HashMap loadYaml(String filename) throws IOException{
        logger.info("loadYaml("+YAML_FOLDER_PATH+filename+")");
        HashMap result = null;
        
        final URL url = DeviceHelper.class
                .getResource(YAML_FOLDER_PATH+filename);
        if (url == null)
            logger.debug("URL of yaml is null");
        else{
            try(InputStream is = url.openStream() ) {
                Yaml yaml = new Yaml();
                //load config file
                result = (HashMap) yaml.load(is);
            }
        }
        return result;
    }
    
    
    /**
     * Load FlashProcess for given Device
     * @param adbDevice
     * @return Process
     * @throws java.io.IOException  
     * @throws java.text.ParseException  
     */
    public static Process loadFlashProcess(String adbDevice) throws IOException, ParseException, NumberFormatException, NullPointerException{

        final Map yamlContent = loadYaml(adbDevice+"_flash.yml");
        final int stepsCount = (Integer) yamlContent.get("stepsCount");
        final Process flashingProcess = new Process(stepsCount);
        HashMap<String, IStep> steps = parseSteps( (HashMap) yamlContent.get("steps") );

        flashingProcess.setSteps(steps );
        
        return flashingProcess;
    }   
}
