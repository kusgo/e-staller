/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models.steps;

import ecorp.easy.installer.models.Command;

/**
 * This concern step with Fixed UI
 * but which run command in Background
 * E.g :Device detection
 * @author vincent Bourgmayer
 */
public class ExecutableStep extends BasicStep implements IExecutableStep{

    private final Command command;
    
    public ExecutableStep(String type, String nextStepKey, int stepNumber, Command command) {
        super(type, nextStepKey, stepNumber);
        this.command = command;
    }

    @Override
    public Command getCommand() {
        return command;
    }
}
