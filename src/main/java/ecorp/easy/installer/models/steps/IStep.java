/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models.steps;

/**
 * Represent a basic Step type
 * 
 * Below comment also apply for child interface
 * 
 * It doesn't include setter signature because
 * without the only place where they could be used
 * is in configParser and since we have constructor it's useless
 * Moreover, it allow to set most of the implementation field "final"
 * 
 * However, if class current implementation should evolve, it will be 
 * a pain to update constructor method to handle new field.
 * So we could consider, if we feel the need to add those setters
 * in interface definition in future. This isn't a closed Topic.
 * @author vincent Bourgmayer
 */
public interface IStep {
    public final static String LAST_STEP_KEY = "end"; //When a next step key has this value, it means that this is the last step of the process
    
    /**
     * Get the type of the step
     * @return 
     */
    public String getType();
    
    /**
     * Get the number of the step in the whole process
     * @return e.g 1, which means 1/5 if there is 5 steps
     * in the whole process
     */
    public int getStepNumber();
    
    /**
     * get the key of the next step to call 
     * @return String
     */
    public String getNextStepKey();
}
