/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 * @author Vincent Bourgmayer
 */
public class Command {
    private final String commandBase; //String containing command to perform
    private String outputKey; //The variable key to use to associate output value
    private final static HashMap<String, String> COMMON_PARAMS = new HashMap<>();
    
    private LinkedHashMap<String, String> parameters; //Parameters to add to the command
    private HashMap<Integer, String> okCodes;
    private HashMap<Integer, String> koCodes;

    /**
     * Instanciate a command object
     * @param commandBase 
     */
    public Command(String commandBase){
        this.commandBase = commandBase;
        this.outputKey = "";
        this.parameters = new LinkedHashMap<>();
        this.okCodes = new HashMap<>();
        this.koCodes = new HashMap<>();
    }
    
    /**
     * Build a command instance with an already existing set of parameters
     * @param commandBase
     * @param parameters 
     */
    public Command(String commandBase, LinkedHashMap<String,String> parameters){
        this.commandBase = commandBase;
        this.parameters = parameters;
        this.outputKey = "";
        this.okCodes = new HashMap<>();
        this.koCodes = new HashMap<>();
    }
    
    /**
     * Map.put(key, value) a new parameter for the current command
     * @param key the key to identify the parameter in the list
     * @param value the value of the parameter
     */
    public void addParameter(String key, String value){
        this.parameters.put(key, value);
    }

    
    /* Getter & setters */
    public String getCommandBase() {
        return commandBase;
    }
    
    /**
     * Return the key to store output 
     * @return 
     */
    public String getOutputKey() {
        return outputKey;
    }

    /**
     * Define the key to store output
     * @param outputKey 
     */
    public void setOutputKey(String outputKey) {
        this.outputKey = outputKey;
    }

    /**
     * Get map of parameters < key, value >
     * @return 
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * Set the map of parameters < key, value >
     * @param parameters 
     */
    public void setParameters(LinkedHashMap<String, String> parameters) {
        this.parameters = parameters;
    }

    /**
     * get map of success code < int, string>
     * where int is the result code
     * and the string the text associated
     * @return 
     */
    public HashMap<Integer, String> getOkCodes() {
        return okCodes;
    }

    public void setOkCodes(HashMap<Integer, String> okCodes) {
        this.okCodes = okCodes;
    }

    public HashMap<Integer, String> getKoCodes() {
        return koCodes;
    }

    public void setKoCodes(HashMap<Integer, String> koCodes) {
        this.koCodes = koCodes;
    }
    
    /**
     * Get list of parameters available for every command
     * @return 
     */
    public static HashMap<String, String> getCOMMON_PARAMS() {
        return COMMON_PARAMS;
    }
    /**
     * Return the message associated with the exit Value
     * @param exitValue
     * @return String can be null if exitValue is not define for this step
     */
    public String getErrorMsg(int exitValue){
        return this.koCodes.getOrDefault(exitValue, null);
    }
}
