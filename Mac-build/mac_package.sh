#!/bin/bash -e

APPNAME=EasyInstaller
DIR="$APPNAME.app/Contents/MacOS"
DIR1="$APPNAME.app/Contents/Resources"

if [ -a "$APPNAME.app" ]; then
	echo "$PWD/$APPNAME.app already exists delete it."
	rm -rf "$APPNAME.app"
fi

mkdir -p "$DIR"
mkdir -p "$DIR1"
cp EasyInstaller "$DIR/$APPNAME"
chmod +x "$DIR/$APPNAME"

cp -r ../build/image/easy-installer-mac/* "$DIR"
cp Info.plist "$DIR/.."
cp easy-installer.png "$DIR1"

mkdir -p "$APPNAME-installer"
mv  "$APPNAME.app" "$APPNAME-installer"

_SIZE=$(du -s EasyInstaller-installer | cut -f 1)

let SIZE=_SIZE/1000+1
echo $SIZE

dd if=/dev/zero of=/tmp/EasyInstaller.dmg bs=1M count=$SIZE status=progress
mkfs.hfsplus -v Install /tmp/EasyInstaller.dmg
mkdir -pv /tmp/mnt-easyinstaller
mount -o loop /tmp/EasyInstaller.dmg /tmp/mnt-easyinstaller
cp -av EasyInstaller-installer/* /tmp/mnt-easyinstaller
umount /tmp/mnt-easyinstaller

rm -rf "$APPNAME.app"
rm -rf EasyInstaller-installer
mv /tmp/EasyInstaller.dmg .
