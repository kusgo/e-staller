How to create an installer (.dmg) for MacOs for the Easy Installer

1/ Build the easy installer application normally
2/ Goto this folder (Mac-build)
3/ run the ./mac_package.sh script. This create a folder `EasyInstaller-installer` containing the `EasyInstaller.app` (application)

4/ sudo apt-get install hfsprogs
5/ dd if=/dev/zero of=/tmp/EasyInstaller.dmg bs=1M count=128 status=progress
6/  mkfs.hfsplus -v Install /tmp/EasyInstaller.dmg
7/ mkdir -pv /tmp/mnt-easyinstaller
8/ mount -o loop /tmp/my_application.dmg /tmp/mnt-easyinstaller
9/ cp -av EasyInstaller-installer/ /tmp/mnt-easyinstaller
 
10/ umount /tmp/mnt-easyinstaller




7/ The installer is ready
8/ TODO : The Info.plist file should contains the version number

How to install the EasyInstaller on MacOS
* see https://gitlab.e.foundation/e/devices/easy-installer/-/wikis/MacOS-install

